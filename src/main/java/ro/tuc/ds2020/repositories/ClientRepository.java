package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.ApplicationUser;
import ro.tuc.ds2020.entities.Client;

public interface ClientRepository extends JpaRepository<Client, Long>{
    Client findClientByUser(ApplicationUser user);
}
