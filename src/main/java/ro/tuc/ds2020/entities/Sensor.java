package ro.tuc.ds2020.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import lombok.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Sensor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "sensor_description", nullable = false)
    private String sensorDescription;

    @Column(name = "maximum_value", nullable = false)
    private long maximumValue;

    @OneToOne
    @JoinColumn(name="device_id",referencedColumnName = "id",unique = true)
    private Device device;
}
