package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Date;
import lombok.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class MonitoredValue{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "time_stamp", nullable = false)
    private Date timeStamp;

    @Column(name = "energy_consumption", nullable = false)
    private long energyConsumption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="sensor_id", nullable=false)
    private Sensor sensor;
}
