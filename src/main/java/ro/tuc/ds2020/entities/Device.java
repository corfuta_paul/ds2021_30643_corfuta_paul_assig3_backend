package ro.tuc.ds2020.entities;

import javax.persistence.*;
import javax.persistence.Entity;
import lombok.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor

public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "maximum_energy_consumption", nullable = false)
    private long maximumEnergyConsumption;

    @Column(name = "average_energy_consumption", nullable = false)
    private long averageEnergyConsumption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="client_id", nullable=false)
    private Client client;
}
