package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ClientDTO extends RepresentationModel<ClientDTO> {
    private long id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String address;
    private long user_id;
}
