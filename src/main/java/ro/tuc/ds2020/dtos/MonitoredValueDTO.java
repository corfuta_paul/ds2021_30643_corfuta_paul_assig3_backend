package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor

public class MonitoredValueDTO extends RepresentationModel<MonitoredValueDTO> {

    private long id;
    private Date timeStamp;
    private long energyConsumption;
    private long sensor_id;
}
