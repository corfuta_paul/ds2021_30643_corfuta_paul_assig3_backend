package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.entities.MonitoredValue;
import ro.tuc.ds2020.entities.Sensor;

public class MonitoredValueBuilder {
    private MonitoredValueBuilder(){

    }

    public static MonitoredValueDTO ToMonitoredValueDTO(MonitoredValue monitoredValue){
        return new MonitoredValueDTO(
                monitoredValue.getId(),
                monitoredValue.getTimeStamp(),
                monitoredValue.getEnergyConsumption(),
                monitoredValue.getSensor().getId()
        );
    }

    public static MonitoredValue ToEntityMonitoredValue(MonitoredValueDTO monitoredValueDTO, Sensor sensor){
        return new MonitoredValue(
                monitoredValueDTO.getId(),
                monitoredValueDTO.getTimeStamp(),
                monitoredValueDTO.getEnergyConsumption(),
                sensor
        );
    }
}
