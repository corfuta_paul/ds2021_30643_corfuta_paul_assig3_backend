package ro.tuc.ds2020.Server;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;
import org.springframework.web.bind.annotation.RequestBody;
import ro.tuc.ds2020.MeasurementService.MonitoredValueService;
import ro.tuc.ds2020.services.UserService;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@RequiredArgsConstructor
public class Server {
    private final ro.tuc.ds2020.services.MonitoredValueService monitoredValueService;
    private final UserService userService;
    @Bean(name = "/monitoring")
    RemoteExporter monitoringService() {
        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setService(new MonitoredValueServiceImpl(monitoredValueService,userService));
        exporter.setServiceInterface( MonitoredValueService.class );
        return exporter;
    }

}
