package ro.tuc.ds2020.Server;

import lombok.RequiredArgsConstructor;
import ro.tuc.ds2020.MeasurementService.MonitoredValue;
import ro.tuc.ds2020.MeasurementService.MonitoredValueException;
import ro.tuc.ds2020.MeasurementService.MonitoredValueService;
import ro.tuc.ds2020.dtos.MonitoredValueDTO;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.ApplicationUser;
import ro.tuc.ds2020.services.UserService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class MonitoredValueServiceImpl implements MonitoredValueService {
    private final ro.tuc.ds2020.services.MonitoredValueService monitoredValueService;
    private final UserService userService;
    @Override
    public ArrayList<MonitoredValue> monitorValue(String username,String password) throws MonitoredValueException {

          List<ro.tuc.ds2020.entities.MonitoredValue> monitoredValues;
          ArrayList<MonitoredValue> monitoredValues1= new ArrayList<>();

         UserDTO user_logged=userService.login(new UserDTO(0,username,password,false));

         if(user_logged!=null) {
            monitoredValues = monitoredValueService.findMonitoredValuesByUserId(UserBuilder.ToEntityUser(user_logged));
             for (ro.tuc.ds2020.entities.MonitoredValue mv:
             monitoredValues) {
            String timeStamp= mv.getTimeStamp().toString();
            String energyConsumption= String.valueOf(mv.getEnergyConsumption());
            monitoredValues1.add(new MonitoredValue(timeStamp,energyConsumption));
         }
            return monitoredValues1;
         }
           return null;
    }
}
