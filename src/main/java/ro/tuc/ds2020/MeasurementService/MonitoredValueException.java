package ro.tuc.ds2020.MeasurementService;

public class MonitoredValueException extends Exception {
    public MonitoredValueException(String message) {
        super(message);
    }
}