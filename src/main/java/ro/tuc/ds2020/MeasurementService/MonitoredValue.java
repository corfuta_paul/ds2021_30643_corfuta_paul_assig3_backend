package ro.tuc.ds2020.MeasurementService;

import java.io.Serializable;

public class MonitoredValue implements Serializable {
    private String timeStamp;
    private String energyConsumption;


    public MonitoredValue(String timeStamp, String energyConsumption) {
        this.timeStamp=timeStamp;
        this.energyConsumption=energyConsumption;

    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getEnergyConsumption() {
        return energyConsumption;
    }
}
